all: ebook site

clean: nobook nosite

anew: clean all

allhtml: page/boekhouden.html page/communiceren.html page/geld.html page/houding.html page/passie.html page/rekenen.html page/stappen.html page/vrijheid.html page/titel.html page/colofon.html

allmd: mark/index.md mark/boekhouden.md mark/communiceren.md mark/geld.md mark/houding.md mark/passie.md mark/rekenen.md mark/stappen.md mark/vrijheid.md mark/colofon.md

mark/%.md: docs/%.rst
	mkdir -p mark
	pandoc -f rst -t markdown $< > $@

mark/index.md: docs/titel.rst
	mkdir -p mark
	pandoc -f rst -t markdown $< > $@

page/%.html: docs/%.rst
	mkdir -p page
	rst2html --language=nl $< $@
	tidy -asxhtml -language nl -m $@

ebook: allhtml ebook.ncx ebook.opf
	rm -f zzp.epub
	zip -q0X   zzp.epub mimetype
	zip -qXr9D zzp.epub mimetype META-INF ebook.* page/*.html

nobook:
	rm -f page/*.html zzp.epub

site: allmd mkdocs.yml
	mkdocs build

nosite:
	rm -f mark/*.md

