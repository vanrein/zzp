Boekhouden kun je zelf
======================

Een klein bedrijf kun je zonder veel problemen zelf boekhouden.  Je moet wel
dingen weten zoals welke kosten je mag opvoeren (een nieuwe printer) en welke
niet (een armanipak) -- dat kun je allemaal nalezen in het handboek voor
ondernemers dat de Belastingdienst op de website heeft staan.  Lees het van
begin tot eind door, het is taaie maar nuttige kost.

Als je onzeker bent dan kun je altijd met de Belastingdienst bellen.  Ze zijn
er om je te helpen en denken zo goed mogelijk met je mee.  Je zult merken
dat je veel meer zelf kunt kiezen dan met je priveaangifte.  Eigenlijk is
het alleen zo dat misbruik wordt verboden, maar dat je verder datgene kunt
doen wat je zakelijk slim lijkt.  Leef naar de geest van de voorschriften
en je zult weinig problemen met de Belastingdienst hebben.

Een boekhouding kan meestal bestaan uit een paar simpele kolommen::

  NUMMER OMSCHRIJVING                        BIJ   BTW   AF    BTW

  FI-1   Inschrijving KvK                                60   
  FI-2   Website voor 1 jaar                             25    5
  FO-1   Geholpen met project "Lust of Last"  50   10
  FO-2   Cursus gegeven aan "Zon & Zee"      200   43



         ----------------------------------- ----- ----- ----- -----
         Totaal na 1e kwartaal 2023          250   53    85    5

Je kunt zelf kiezen of je dit op papier wilt doen of, als je veel facturen
verstuurt of ontvangt, op de computer.  Als je op de computer werkt is het
een goed idee om een boekhoudpakket te leren gebruiken.  GnuCash is een
gratis pakket dat heel geschikt is, maar als je een cursus doet dan wordt
vrijwel altijd geldkostende software gepresenteerd.

We hebben in dit voorbeeld afgerond op hele Euro's maar in het echt werk
je tot op de cent.  Afronden in je voordeel doe je pas op de aangifte.
De bedragen onder BIJ en AF zijn exclusief BTW, en daar reken je normaal
mee omdat BTW voor bedrijven toch met de Belastingdienst wordt verrekend.

Zie je de nummers in de eerste kolom?  Daar staat welk document in je
facturenmap gekoppeld is aan deze regel.  De facturen in je mappen gelden
als bewijsstuk voor de Belastingdienst.  Op dezelfde manier gelden je
poststukken als bewijsstuk tegenover allerlei partijen waarmee je
communiceert.

Dit eenvoudige kasboek is meestal voldoende informatie om als ZZP'er al je
belastingaangiftes te kunnen doen!  De enige uitzonderingen zijn als je
dingen vrij van BTW, tegen 0% BTW of een mengsel van tarieven zou doen.

Op een BTW-aangifte wil men weten::

                                    BEDRAG    BTW
  Omzet belast met 21% BTW          250       53
  Omzet belast met  6% BTW          0         0
  Omzet belast met  0% BTW          0         0
  Omzet vrijgesteld van BTW         0
                                              ---- +
  Totaal ontvangen BTW                        53

  AF: Voorbelasting                            5
                                              ---- -
  Te verrekenen BTW                           48
  AF: Vermindering kleineondernmersregeling   48
                                              ---- -
  Terug te ontvangen of te betalen BTW         0

Let vooral op de kleineondernemersregeling.  Die is voor kleine bedrijven
erg nuttig, want het komt erop neer dat je de BTW mag houden als het onder
een bepaalde grens zit.  Dat verdien je toch maar mooi extra op je klant!

De andere aangifte die je moet doen is de winst uit onderneming.  Die is
al even simpel, en gaat uit van de totalen van het hele jaar::

  Omzet uit zelfstandige onderneming:         250
  Kosten gemaakt voor die omzet:               85
                                              --- -
  Winst uit onderneming:                      165

Op zich simpel zat dus.  Let alleen wel op dat er extra aftrekposten
kunnen zijn voor serieuze zelfstandigen.  De meest voorkomende zijn:

- zelfstandigenaftrek
- startersaftrek
- investeringsaftrek

Enfin, om dit soort dingen te weten is het dus aan te raden het handboek
voor ondernemers goed door te lezen.  Er zijn verscheidene handreikingen
die de Belastingdienst doet om je op gang te helpen als bedrijfje.

