Copyright (c) 2009-2023 Rick van Rein, GroenGemak

Dit werk is beschikbaar onder een royale CC BY-SA 4.0 licentie.

Je mag het werk met anderen delen, ook als je het bewerkt,
onder een voorwaarde van naamsvermelding en het delen onder
dezelfde royale licentie.

Details: https://creativecommons.org/licenses/by-sa/4.0/deed.nl
