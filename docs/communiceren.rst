Communiceren is van levensbelang
================================

Het allerbelangijkste voor jou als bedrijf is dat je helder communiceert
met je klanten en leveranciers.  Wees altijd helder en eerlijk over wat
je vraagt of aanbiedt, en wat je voorwaarden zijn.  Maak notities of
werk schriftelijk, zodat je erop terug kunt komen.  Noteer ook altijd de
naam van je gesprekspartners.

Of het nou gaat om marketing of het inkopen van een dienst, het is in het
algemeen belangrijk dat je wat vaardigheid hebt in het communiceren.  Dat
doe je vaak in het gewone leven op, maar zelfs dan is een cursus nog geen
overbodige luxe.

De essentie van communiceren is aanvoelen wat de ander denkt, voelt of
vindt.  Dat is geen rare truc om misbruik te kunnen maken van die ander,
maar gewoon om je ideeen zo helder mogelijk over te kunnen krijgen, door
de beste aansluiting te zoeken.  Luisteren is essentieel, en zeker zo
belangrijk als praten.  Een verkoper die praat als Brugman kan haast niet
open staan om te begrijpen wat de klant wil.

Of je nou mailt, belt of brieven schrijft: zorg dat je jezelf blijft.
Je kunt bijvoorbeeld netjes en zakelijk blijven zonder afstandelijk te
zijn, als je dat prettig vindt.  Lieg niet en kom daar niet eens
in de buurt, want dat breekt je vroeg of laat op.  Eerlijk duurt het
langst.

Het kan bijvoorbeeld heel vermoeiend zijn om je groter voor te doen dan je bent.  En
het is helemaal niet nodig.  Als je goed bent in wat je doet kun je voor
een groot bedrijf een interessante partij zijn, want jij kunt vermoedelijk
veel flexibeler zijn dan een grotere concurrent.  Jij kunt op verzoek een
speciaal soort netelkaas leveren, terwijl een massakaasmaker alleen de
variatie nagelkaas kent, bijvoorbeeld.



Facturen schrijven
------------------

Doodeng, de eerste rekening sturen.  Maar leuk is het ook, want je plukt de
vruchten van je inspanning, en als het goed is betaalt je klant het nog
met plezier ook.  Omdat je het waard bent.

Op een factuur moeten allerlei zaken staan:

- factuurnummer (uniek en oplopend genummerd)
- BTW-nummer
- KvK-nummer
- contactgegevens (voor als er vragen zijn)
- betaalgegevens: rekeningnummer/tenaamstelling/plaats, betaaltermijn
- specificatie van wat er betaald moet worden
- bedrag exclusief BTW, BTW-bedrag, bedrag inclusief BTW::

    Product X          EUR  25
    Dienst Y           EUR  75
                       ------- +
    Subtotaal          EUR 100
    BTW 21%            EUR  21
                       ------- +
    Totaal te voldoen  EUR 121

Het is handig om een standaardfactuur te hebben, waarin je een markering
zoals het woord TODO gebruikt om invulvelden te markeren.  Je kunt daar dan
naar zoeken zodat je niets vergeet aan te passen.  Overigens kan het
handig zijn om facturen uit een spreadsheet uit te draaien en niet uit een
tekstverwerker.

Stuur je facturen per post, fax of per digitaal ondertekende email.  Houd
bij wanneer er betaald is.  Gebeurt dat niet, bel dan even op en stuur
eventuuel een schriftelijke herinnering.  Meestal is je klant het gewoon
vergeten om een heel menselijke reden, en betalen ze bij herinnering vlot.
Zo niet, dan moet je je afvragen wat er schort -- heb jij iets niet goed
gedaan, of heb je een klant getroffen die je liever kwijt dan rijk bent?


Offertes maken
--------------

Nog enger dan facturen uitschrijven is om een inschatting te maken van wat
een klus moet kosten.  Als je wat grotere klussen doet, dan zal je geregeld
om een offerte worden gevraagd.  Het grote probleem daarbij is een prijs te
vinden die niet zo hoog is dat je klant wegloopt, en niet zo laag dat je er
zelf bij inschiet.  Je zult zelf moeten leren hoe je dat inschat.

De meest veilige inschatting is wel waarbij je inschat hoeveel uren je kwijt
bent, maal je uurloon, en plus je onkosten met een winstmarge.  Dat levert
dan een totaalprijs die je door kunt geven aan je klant.

Denk eraan om particulieren altijd prijzen inclusief BTW te vragen, en aan
bedrijven vermeld je een prijs exclusief BTW.  Dat laatste vermeld je wel
expliciet om gedonder te voorkomen, en vermoedelijk heb je wel meer
voorwaarden waaronder je kunt leveren: je leveringsvoorwaarden.  De offerte
is het document waarbij je die insluit.  Het is vaak een goed idee om dit
soort documenten (zowel offertes als voorwaarden) zo gebalanceerd mogelijk
te schrijven, dus meedenkend met zowel de klant als met jouw belang.

Als je klant je offerte eenmaal heeft ondertekend, dan kun je overgaan
tot de uitvoering.  De ondertekende offerte geldt als contract, dat je
het recht geeft om bij levering facturen te sturen.  Het is dus een
afspraak die voor beide partijen vastlegt wat er gaat gebeuren en voor
welke vergoeding.  Wees daarom altijd helder en eerlijk in een offerte.

