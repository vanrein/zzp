Geld is maar een middel
=======================

Geld moet rollen, liefst naar je toe.  Maar om nou een bedrijf te starten
met als doel om geld te verdienen?  En dan iets anders te beweren tegenover
je klant?

Als je goed bent, dan komt het geld vanzelf.  Tenminste, als je zakelijk
genoeg bent.  Dat is niet eens onnatuurlijk -- als jij een ander ergens
mee helpt dan mag die ander dat best tot uitdrukking brengen door je te
belonen.  Je bent waardevol, en dat gevoel mag je best vasthouden.

Het is ideaal als je geen geldzorgen hoeft te hebben.  Dat gaat vaak het
best als je geen al te hoge hypotheek of andere vaste lasten hebt, en/of
als je een partner hebt met een vaste baan.  Als je bedrijf eenmaal loopt
zul je meer vertrouwen hebben dan wanneer je net begint, dat is logisch.

Wees bereid om keuzes te maken: wil je al je geld uitgeven aan flitsende
kleding of gave gadgets, of is het beter wat achter de hand te houden voor
als je bedrijf even minder loopt?  Dat laatste is vermoedelijk handiger.
Niets zo frustrerend als jezelf fanatiek moeten verkopen omdat je anders
je hypotheek niet kunt betalen.  En niets is zo rustgevend als een potje
geld waar je een aantal maandan van kunt leven als dat zou moeten.

**Minimaliseer je afhankelijkheid van een vast inkomen.**

