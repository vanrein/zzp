Je hoeft echt niet te groeien
=============================

Wil je je voordoen als een groot bedrijf, of durf je het aan om klein te
zijn?  En klein te blijven?  Feit is dat er veel meer overhead komt als
je groeit.  Er wordt weleens gezegd dat het ergste dat je een ondernemer kunt
wensen 10 man personeel is.

Er is niets mee mis om klein te zijn, en te blijven.  Zolang je je vasthoudt
aan wat jij graag wilt en wat je goed kunt is het heel helder waar je voor
staat, en wat je dus te bieden hebt.  Bij een groter bedrijf wordt dat vaak
een stuk vager, en de kwaliteit wordt ook veel meer uitgesmeerd tot een soort
vage algemeenheid.  Want niet iedereen in een groot bedrijf kan alles even
goed.

Ieder jaar meer omzet, het is leuk voor investeerders maar als klein bedrijf
moet je je afvragen of het nodig is.  Als jij precies doet wat je leuk vindt,
en daar je brood mee verdient, wat is daar mee mis?  Laat de economen en
bedrijfskundigen maar praten -- die gaan per definitie niet over de inhoud
van een bedrijf, en die weten dus nauwelijks wat passie is.

Patenten, da's ook zo'n kreet waar economen mee smijten.  Waar het vaak op
neerkomt is dat iemand een idee heeft, en bang is nooit weer een idee te
krijgen, en daarom kiest om zijn idee voor de rest van zijn leven in dure
rechtszaken te bevechten.  Je kunt met een patent namelijk anderen die
hetzelfde idee hebben uitmelken, en dwingen je geld te betalen.  Dat heeft
puur met geld te maken, maar niet met je passie voor wat je doet.  Negeer
het hele patentgebeuren en je bespaart jezelf een hele hoop papierwerk.

Een klein bedrijf kan veel bescheidener van start dan een groot.  We
leggen hier uit hoe eenvoudig het is, en hoe weinig het kost.  Als je ook
niet te veel hoeft te investeren in je bedrijf (eerst maar eens een pand
huren in plaats van kopen bijvoorbeeld) dan hoef je soms niet eens geld
van een bank te lenen.  Dat scheelt een hele hoop sores!

**Schoenmaker, blijf bij je leest.**
