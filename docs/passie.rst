Ondernemen doe je vanuit een passie
===================================

Je moet geen bedrijf beginnen omdat je geld wilt verdienen.  Je moet een
bedrijf beginnen omdat je een droom hebt.  Een droom om vrij te zijn is
niet genoeg: het moet ergens over gaan.  Iets waar je een ander mee kunt
helpen.

Het maakt niet uit of je schoenen wilt verkopen of wiskundige analyses
aan de bewegingen van sporters wilt doen.  Zolang je hart er maar naar
uitgaat.  Iets wat lijkt op een uit de klauwen gegroeide hobby, waar je
graag tijd in steekt en waar je voor anderen iets nuttigs kunt betekenen.

Voeg waarde toe.  Als je goedkoop inkoopt en met winst verkoopt dan komt er
een tijd dat een concurrent je dwingt je marges te verlagen.  Maar als jij
niet alleen plantjes verkoopt maar ze ook uitplant in een border volgens
een creatief ontwerp dan komen je klanten terug -- niet voor de laagste
prijs van planten, maar voor datgene wat je eraan toevoegt.  Datgene wat
jouw bedrijf tot jouw eigen handel maakt.

Het zou al snel onnatuurlijk voelen als je bedrijf andere idealen nastreeft
dan jij persoonlijk.  Als je prive tegen kinderslavernij bent zou het wel
raar zijn om koffie en cacao in te voeren waarbij dat schering en inslag
is.  En als je van koken houdt moet je geen franchisenemer van een
fastfoodketen worden.  Je bedrijf en jij, da's hetzelfde.  Of, preciezer,
je bedrijf is je kindje, en die voed je op naar je eigen inzichten.

**Je bedrijf is een excuus om een passie uit te leven.**
