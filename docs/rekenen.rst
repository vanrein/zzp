Hoeveel levert je werk op?
==========================

Als je iets wilt gaan verkopen, is het goed om in te schatten of het uit kan.
Daar kun je heel ingewikkeld over doen, maar een vlotte krabbel achterop
een envelop kan even goed werken als een uitgebreide spreadsheet.

Je zult een idee moeten hebben (of uitproberen) van wat je wilt vragen.
Prijzen worden niet bepaald door inkoopprijs plus een marge, maar door
wat je voor je klanten waard bent.

Als je klanten particulieren zijn dan moet je ze prijzen inclusief BTW geven,
als het zakelijke klanten zijn dan reken je prijzen exclusief BTW voor.
Particuliere prijzen moet je daarom 21/121 deel aftrekken om te
komen tot de prijs ex BTW (ook wel de omzet genoemd) die in je boekhouding
terechtkomt.  We gaan er van uit dat de BTW naar de Belastingdienst verdwijnt.

Van de prijs trek je de kosten af die je ervoor maakt.  Dat is een beetje
koffiedikkijken, want je maakt vaak vaste kosten die je moet verdeln over
alle verkopen, en variabele kosten per verkoop.   Omzet min kosten, dat
heet je winst.

De winst komt bij je box 1 op.  Daar wordt vervolgens inkomstenbelasting
op ingehouden.  Gemiddeld is dat zo'n 40%.  Wat daarna overblijft heb je
netto in het handje, om uit te geven.

De hele berekening ziet er als volg uit::

  Consumentenprijs        EUR 182
  AF: BTW 21/121 deel     EUR  32
                          ------- -
  Omzet (dus ex BTW)      EUR 150
  Kosten                  EUR  50
                          ------- -
  Winst                   EUR 100
  Inkomstenbelasting 40%  EUR  40
                          ------- -
  Netto verdiensten       EUR  60

Haal maar even diep adem.  Je houdt soms maar een derde over van wat de klant
je betaalt, als dat een consument is.  Dat komt mede door de kosten, die per
geval verschillen.  Daar staat dan wel tegenover dat je ze kunt aftrekken,
en dat is dubbel zo voordelig als het van je nettoloon te betalen, zoals je
misschien deed toen het gewoon een hobby was.

Omdat deze berekening per produkt of dienst sterk varieert moet je altijd
even zo'n berekeningetje maken, al is het maar achterop een oude envelop.
De vraag die je jezelf moet stellen is of de verdiensten je hoog genoeg
zijn voor deze verkoop.  Als je er bijvoorbeeld 4 uur werk voor moet verrichten
dan kom je uit op een netto uurloon van EUR 15.  Klopt dat met je verwachtingen?

De reden dat een uurloon nuttig is om uit te rekenen, is dat je je moet
afvragen of je ervan kunt leven als je voltijds bezig zou zijn om dit ene
ding te verkopen.  Ben je er tevreden mee dan kun je het product gaan
verkopen.  Anders kun je het beter laten, tenzij je verwacht dat je het
sneller of tegen lagere kosten kunt doen als je dit aan de lopende band
verkoopt.

Bovenstaande berekening ziet eruit alsof je nooit op gang zult kunnen komen,
maar daar is wel een regeling voor bedacht.  Als je ongeveer halftijds of
meer aan je bedrijf werkt, dan voldoe je aan het urencriterium van de
Belastingdienst en mag je zelfstandigenaftrek (en in de beginjaren ook nog
startersaftrek) toepassen.  Als je maar inkomsten hebt dan levert dat een
aftrekpost die zo hoog is als een ruime hypotheek.

