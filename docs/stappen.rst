Stap voor stap naar ondernemersschap
====================================

Het is niet zo moeilijk om een eigen bedrijf op te zetten.  Het is echter
wel belangrijk dat je het doet, al was het maar om jezelf en de rest van de
wereld te vertellen dat het menens is met je plannen.  Dat je er nu echt
voor gaat.

1. Beslis **wat je wilt doen**.

   Omschrijf voor jezelf in een paar zinnen wat je wilt doen.  Wat is dat,
   die passie van je?  Je zit hier niet aan vast, maar het helpt enorm om
   helder te krijgen wat je wilt.  Laat je niets wijsmaken, maar volg je
   hart.

2. **Registreren** bij KvK en Belastingdienst.

   Je kiest een bedrijfsnaam die nog niet bij de KvK bekend is in de regio
   waarin je wilt handelen.  Als je die naam aanmeldt is die verder van jou.
   Mocht je ooit een website willen maken, kijk dan ook meteen of de naam
   daarvan vrij is, en reserveer die ook meteen.  Zo duur is het niet, en
   niets is zo vervelend als een ander die online jouw naam voert.

   Als je je aanmeldt bij de KvK dan krijg je binnen een paar dagen een
   formulier van de Belastingdienst.  Daarna krijg je meestal een
   BTW-nummer toegestuurd.  Je KvK- en BTW-nummers horen in je brievenboek
   thuis.

   Vergeet niet een feestje te vieren zodra je bedrijf geregistreerd is.
   Beschuit met muisjes is geen gek idee!

3. Regel je **bereikbaarheid**.

   Zorg liefst voor een eigen telefoonnummer voor je werk, zodat je kunt
   horen of je prive of zakelijk wordt gebeld.  Het is wel zo duidelijk om
   met je bedrijfsnaam op te nemen als klanten bellen.  Een hele simpele
   manier om dit te doen is via een SIP-telefoon en ditto nummer.  Dat sluit
   je aan op een netwerk en voila, je kunt bellen en gebeld worden.

   Een website en email is voor bijna alle bedrijven een goed idee.
   Teksten op je website met daarin de woorden die jou beschrijven zijn
   nuttig omdat je klanten op die woorden zoeken op het Internet.  En een
   email is vaak handiger dan een telefoonnummer, omdat je niet onmiddellijk
   hoeft te reageren.

   Als je toch aan het ontwerpen bent, ga dan meteen netjes om met de fonts
   en kleuren die je gebruikt, en laat die overal terugkomen.  Ook op je
   briefpapier bijvoorbeeld.  Een eigen huisstijl kan zo simpel zijn als
   een consequent beleid met lettertypes en kleurvlakken.  Een logo kan, maar
   is niet noodzakelijk.  Maak een standaardlayout voor briefpapier die je
   zo in je favoriete tekstverwerker inlaadt.

   Ontwerp ook visitekaartjes.  Doe niet origineel als dat onhandig uitpakt.
   De achterkant moet bijvoorbeeld beschrijfbaar zijn.  Het formaat is
   85mm bij 55mm, en daar zijn allerlei bewaarsysteempjes op ontworpen.
   Bepaal ook of je wilt strooien met je kaartjes, of dat je ze alleen wilt
   uitgeven aan serieuze contactpersonen.  Ga altijd zorgvuldig om met de
   kaartjes van een ander, en noteer op de achterkant kort waar en wanneer
   je diegene trof, en waarover je gesproken hebt.

4. Regel **hardware en software**

   Zorg dat je een computer hebt die niet door je kinderen wordt gebruikt.
   Je wilt je boekhouding niet kwijtraken omdat een spelletje kuren
   vertoont!  Een kleurenlaserprinter is vaak handig, en spaart op den
   duur geld uit ten opzichte van een inkjetprinter.  Bovendien ziet het
   printwerk er professioneel uit.

   Kies voor open source software.  Bijvoorbeeld kun je met MS Word niets
   maken dat je met goed fatsoen als document kunt verzenden, terwijl je
   met OpenOffice gewoon PDF's exporteert die prima via mail kunnenn worden
   verzonden.  Je komt op die manier zakelijker en professioneler over.
   Maar het belangrijkste voordeel van open source software is dat het
   bijna altijd gratis is, zodat je geen problemen hebt met licenties
   en beperkingen voor zakelijk gebruik.

   Zorg voor een werkplek waar je rustig kunt werken, zonder afleidingen
   van je dagelijkse leven.  Een buro met telefoon en computer en een
   (telefoon)notitieblok is nuttig, en een kast met de mappen waarin je
   je administratie voert.

   Je moet vast nog meer regelen, maar dat verschilt per bedrijf.  Je moet
   zelf bepalen wat je nodig hebt.  Start bescheiden, en ga niet als een
   dolle geld uitgeven dat je nooit weer terugverdient.  Bedenk je dat een
   bedrijf veel ontspannener is als je alles zelf financiert, en je niet
   op allerlei manieren in de schulden hoeft te steken.  Dat kun je niet
   altijd voorkomen, maar het zo veel mogelijk voorkomen is altijd handig.

5. Start een **administratie**

   Dit klinkt heftig, maar is het niet.  Je wilt in mappen, achter
   tabbladen, de volgende soorten dingen gescheiden opslaan, liefst met een
   nummering zoals FI-123 per soort; ik geef de soorten korte codes:

   - PI: inkomende poststukken/brieven
   - PO: uitgaande poststukken/brieven
   - FI: inkomende facturen/rekeningen
   - FO: uitgaande facturen/rekeningen

   Afhankelijk van je bedrijf kan er meer nodig zijn, maar deze basis is
   voor elk bedrijf nuttig.  Stop per soort document voorin de map een
   vel papier met daarop elk nummer op een regel, met daarachter een
   korte beschrijving.  Dat zoekt veel sneller dan bladeren in een dikke
   map:

   - PO-11: Aangifte BTW, 2023-Q1
   - PO-12: Offerte naar Jansen & Janssen

   en elders:

   - PI-26: Offerte akkoord van Jansen & Janssen
   - PI-27: Aanslag BTW, 2023-Q1

   Het is handig om de inhoud van de mappen per tabblad in omgekeerde
   volgorde te doen.  Dus brief PO-12 zit voor brief PO-11.  Dat bladert en
   zoekt net even handiger.  Stukken met bonnetjes eraan (vaak je eigen
   declaraties, ofwel inkomende facturen) kun je het beste helemaal
   achterin stoppen.

   Krijg je een stuk binnen of verstuur je er een, stop hem dan meteen in
   de juiste map.  Geef het nieuwe stuk een nummer, bijvoorbeeld FI-123
   voor factuur in nummer 123, schrijf hem meteen bij op de overzichtspagina
   en stop hem op de juiste plek in de map.  Houd dit bij en je hebt nooit
   problemen om dingen terug te vinden.  Ook als je tijdelijk een stuk uit
   de map haalt moet je ernaar streven hem er zo snel mogelijk weer in te
   stoppen -- laat de map bijvoorbeeld openliggen op die plek zodat je het
   niet vergeet.  Je wilt voorkomen dat er stukken kwijtraken.

