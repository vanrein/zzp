Ondernemen is vrij zijn
=======================

Een belangrijk voordeel van het hebben van een eigen bedrijf is dat je
veel meer vrijheid krijgt.  Ook meer werk, want er komt veel meer bij
kijken dan bij een baas werken, maar juist dat kan heel inspirerend
zijn, omdat je het allemaal op jouw manier kunt doen.

Toch ben je niet helemaal vrij.  Je hebt klanten die je tevreden wilt
houden, zodat ze nog eens terugkomen.  Je hebt de Belastingdienst die
van je verlangt dat je aangiftes doet.  Je zult redelijk vlot willen
kunnen reageren op telefoontjes.

Maar buiten die dingen om heb je de vrijheid om precies die dingen te
doen die je leuk lijken, en die na enig peinzen nuttig genoeg zijn voor
anderen om ze te verkopen.  Dingen die passen bij wie je bent en hoe je
in het leven staat.

Het is hartstikke leuk om contacten te leggen met andere bedrijven,
klein en groot, en te proberen iets op te zetten waar je allebei beter
van wordt.  Je kunt het synergie noemen als je van mooie woorden houdt,
maar je kunt er ook gewoon van genieten.  Niets zo leuk als merken dat
je een nuttig radertje vormt in de maatschappij.  Jij, met jouw idealen
en met dat nieuwe plannetje dat vorige week nog niet bestond.

